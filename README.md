# Observability Example

This is example project for Bright Invention's blogpost.

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app.<br />
Open [http://localhost:8080/hello](http://localhost:8080/hello) to view it in the browser.

import express from "express";

const app = express();

app.get('/hello', (req, res) => {
    res.send('Hello World!');
    console.log("Hello World log!")

});

app.listen(8080, () => {
    console.log('Example app listening on port 8080! Access it: http://localhost:8080/hello');
});